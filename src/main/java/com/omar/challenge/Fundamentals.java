package com.omar.challenge;

import com.google.common.collect.ImmutableList;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Fundamentals {

    private Fundamentals() {

    }

    /*1. Array.dif*/
    public static int[] arrayDiff2(int[] a, int[] b) {
        if (b.length == 0) return a;
        ArrayList<Integer> temp = new ArrayList<>();
        for (int valueA : a) {
            boolean contains = false;
            for (int valueB : b) {
                if (valueA == valueB) {
                    contains = true;
                    break;
                }
            }
            if (!contains) temp.add(valueA);
        }
        int[] result = new int[temp.size()];
        Iterator<Integer> iterator = temp.iterator();
        int position = 0;
        while (iterator.hasNext()) {
            int valueTemp = iterator.next();
            result[position] = valueTemp;
            position++;
        }
        return result;
    }

    public static int[] arrayDiff(int[] a, int[] b) {
        List<Integer> listB = Arrays.stream(b).boxed().collect(Collectors.toList());


        return Arrays.stream(a)
                //.boxed()
                .filter(digit -> !listB.contains(digit))
                //.mapToInt(Integer::intValue)
                .toArray();
    }

    /*2. Convert boolean values to strings 'Yes' or 'No'.*/
    public static String boolToWord(boolean b) {
        return b ? "Yes" : "No";
    }

    /*3. Isograms*/
    public static boolean isIsogram(String str) {
        return str.length() == str.toLowerCase().chars().distinct().count();
    }

    /*4. remove */
    public static String remove(String string) {

        return string.substring(1, string.length() - 1);
    }

    /*5. Descending Order*/
    public static int reverseInteger(final int num) {

       /* return Integer.parseInt(String.valueOf(num)
                .chars()
                .mapToObj(digit -> String.valueOf(Character.getNumericValue(digit)))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining()));*/

          String reversed = Arrays.stream(String.valueOf(num).split(""))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining());

          return Integer.parseInt(reversed);


    }

    //6. StringEndsWith
    public static boolean endsWhit(String str, String ending) {
        return str.endsWith(ending);
    }

    //7.  Odd or Even
    public static String oddOrEven(int[] array) {
        return (Arrays.stream(array).sum() % 2 > 0) ? "odd" : "even";

    }

    //8.  Reverse words
    public static String reverseWords(final String original) {
        if (original.trim().isEmpty()) return original;

        return Arrays.stream(original.split(" "))
                .map(StringBuilder::new)
                .map(StringBuilder::reverse)
                .collect(Collectors.joining(" "));


    }

    //9. Sum between numbers
    public static int getSum(int a, int b) {
        if (a == b) return a;

        int sum = a;
        if (a < b) {
            while (a < b) {
                a++;
                sum = sum + a;
            }
        } else {
            while (a > b) {
                a--;
                sum = sum + a;

            }
        }
        return sum;
        /* use Euler's formula to sum up all numbers from 0 to bigger
          /  and subtract the sum of numbers from 0 to smaller (exclusive)
          */
        //return (a + b) * (Math.abs(a - b) + 1) / 2
    }

    //10. Keep Hydrated!
    public static int liters(double time) {
        return (int) Math.floor(time * 0.5);
    }

    //11. Count the smiley faces!
    public static int countSmileys(List<String> list) {

        Predicate<String> hasMinimumFace = face -> face.length() <= 3;
        Predicate<String> hasEyes = face -> face.startsWith(":") || face.startsWith(";");
        Predicate<String> hasRightNose = face -> face.length() == 3 ? List.of("-", "~").contains(face.substring(1, 2)) : true;
        Predicate<String> hasSmile = face -> face.endsWith(")") || face.endsWith("D");

        Predicate<String> completeRequisites = hasMinimumFace.and(hasEyes).and(hasRightNose).and(hasSmile);
        return (int) list.stream()
                .filter(completeRequisites)
                .count();

        // return (int)arr.stream().filter( x -> x.matches("[:;][-~]?[)D]")).count();

    }

    public static String[] filterWords(String[] words) {

        return Arrays.stream(words)
                .filter(word -> word.startsWith("a"))
                .filter(word -> word.endsWith("b"))
                .toArray(String[]::new);

    }

    public String truncateSentence(String s, int k) {
        return Arrays.stream(s.split(" "))
                .limit(k)
                .collect(Collectors.joining(" "));


    }

    public static int removeDuplicates(int[] nums) {


        int[] newNums = Arrays.stream(nums)
                .boxed()
                .distinct()
                .mapToInt(Integer::intValue)
                .toArray();
        int[] a = Arrays.stream(nums)
                .distinct()
                .toArray();

        nums = Arrays.copyOf(newNums, newNums.length);

        return a.length;
    }


}
