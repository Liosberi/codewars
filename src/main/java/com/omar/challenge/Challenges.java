package com.omar.challenge;


import com.omar.model.ListNode;
import com.omar.utilities.Utilities;
import lombok.var;

import java.math.BigInteger;
import java.util.*;


import static java.util.Collections.swap;
import static java.util.Optional.ofNullable;

import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Challenges {
    static int longestPalindromeStart = 0;
    static int longestPalindromeLength = 0;

    public static int trailingZeros(Integer n) {
        BigInteger result;
        if (n <= 0) return 0;
        BigInteger newN = BigInteger.valueOf(n);
        result = Utilities.calculateFactorial(newN);
        String iterator = result.toString();

        int count = 0;
        for (int i = iterator.length() - 1; i >= 0; i--) {
            char c = iterator.charAt(i);
            if ('0' == (c)) {
                count++;
            } else {
                break;
            }

            //return count;
        }

        return (int) Arrays.stream(iterator.split(""))
                .filter(l -> l.equals("0"))
                .count();
    }

    /*check if it's prime next time use square  */
    public static boolean isPrime(int num) {
        if (num <= 1) return false;
        for (int x = 2; x < num / 2; x++) {
            if (num % x == 0) return false;
        }
        return true;
    }

    public static int getBalance(int[] transactions, String[] dates) {
        Map<String, List<Integer>> transactionsPeerMonth = new HashMap<>();
        int initialBalance = Arrays.stream(transactions).sum();
        int annualFee = 5;
        int monthsWithDiscount = 12;
        final int[] orderTransaction = {0};
        Arrays.stream(dates)
                .map(d -> d.substring(5, 7))
                .forEach(month -> {
                    int transactionValue = transactions[orderTransaction[0]];
                    List<Integer> monthTransactions = new ArrayList<>(
                            ofNullable(transactionsPeerMonth.get(month))
                                    .orElseGet(Collections::emptyList));
                    monthTransactions.add(transactionValue);
                    transactionsPeerMonth.put(month, monthTransactions);
                    orderTransaction[0]++;
                });


        for (List<Integer> lists : transactionsPeerMonth.values()) {
            Predicate<Integer> isCardPayment = values -> values < 0;
            long numberOfCardPayments = lists.stream().filter(isCardPayment).count();
            int balanceInMonth = lists.stream().filter(isCardPayment).mapToInt(Integer::intValue).sum();
            if (balanceInMonth <= -100 && numberOfCardPayments >= 3) {
                monthsWithDiscount--;
            }
        }
        return initialBalance - (annualFee * monthsWithDiscount);
    }

    public static String createPhoneNumber(int[] numbers) {

        String numbersString = Arrays.stream(numbers)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining());

              String result = "(".concat(numbersString.substring(0, 3))
                .concat(") ")
                .concat(numbersString.substring(3, 6))
                .concat("-")
                .concat(numbersString.substring(6, 10));
        return result;
    }

    public static int sequence(int[] arr) {
        //-2, 1, -3, 4, -1, 2, 1, -5, 4
        int max_ending_here = 0, max_so_far = 0;
        for (int v : arr) {
            max_ending_here = Math.max(0, max_ending_here + v);
            max_so_far = Math.max(max_so_far, max_ending_here);
        }
        return max_so_far;
    }

    //static int initialPosition = 0;

    public static Set<String> singlePermutations(String word) {
        Set<String> permutations = new HashSet<>();
        permutation(word, 0, permutations);
        return permutations;

    }

    public static void permutation(String word, int initialPosition, Set<String> permutations) {
        List<String> letters = Arrays.stream(word.split("")).collect(Collectors.toList());
        /*if (letters.size() - initialPosition == 1){
            permutations.add(word);

        }*/
        for (int i = initialPosition; i < letters.size(); i++) {
            swap(letters, initialPosition, i);
            permutation(letters.stream().collect(Collectors.joining()), ++initialPosition, permutations);
            swap(letters, i, --initialPosition);
        }
        permutations.add(word);
    }

    public static int[] twoSum(int[] numbers, int searchedNumber) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        for (int i = 0; i < numbers.length; i++) {
            int number = numbers[i];
            int expected = searchedNumber - number;
            if (map.containsKey(expected)) {
                return new int[]{map.get(expected), i};
            } else {
                map.put(number, i);
            }
        }
        return new int[]{-1, -1};
    }

    public static int romanToInt(String s) {
        Map<Character, Integer> romanNumbers = Map.of('I', 1, 'V', 5, 'X', 10, 'L',
                50, 'C', 100, 'D', 500, 'M', 1000);
        int solution = 0;
        for (int i = 0; i < s.length(); i++) {
            if (i > 0 && romanNumbers.get(s.charAt(i)) > romanNumbers.get(s.charAt(i - 1))) {
                solution += romanNumbers.get(s.charAt(i)) - (2 * romanNumbers.get(s.charAt(i - 1)));//rest twice before number due you already add it
            } else {
                solution += romanNumbers.get(s.charAt(i));
            }
        }
        return solution;
    }

    public static List<String> fizzBuzz(int n) {
        List<String> solution = new ArrayList<>();

        for (int i = 1; i <= n; i++) {
            String value = "";
            if (i % 3 == 0 && i % 5 == 0) {
                value = "FizzBuzz";
            } else if (i % 3 == 0) {
                value = "Fizz";
            } else if (i % 5 == 0) {
                value = "Buzz";
            } else {
                value = String.valueOf(i);
            }
            solution.add(value);
        }
        return solution;
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carry = 0;
        ListNode emptyNode = new ListNode(0);
        ListNode currentNode = new ListNode(0);
        ListNode result = currentNode;
        while (emptyNode != l1 || emptyNode != l2) {
            int sum = l1.val + l2.val + carry;
            carry = sum / 10;
            currentNode = (currentNode.next = new ListNode(sum %= 10));
            l1 = null != l1.next ? l1.next : emptyNode;
            l2 = null != l2.next ? l2.next : emptyNode;
        }
        if (carry == 1)
            currentNode.next = new ListNode(carry);
        return result.next;
    }

    public static int lengthOfLongestSubstring(String s) {
        int pointerA = 0;
        int pointerB = 0;
        int max = 0;

        HashSet<Character> letters = new HashSet<>();
        while (pointerB < s.length()) {
            char goTo = s.charAt(pointerB);
            if (!letters.contains(goTo)) {
                letters.add(goTo);
                max = Math.max(letters.size(), max);
                pointerB++;
            } else {
                char keep = s.charAt(pointerA);
                letters.remove(keep);
                pointerA++;

            }
        }
        return max;
    }

    public static boolean isPalindrome(int x) {

        if (x < 0) return false;
        String number = String.valueOf(x);
        String reversed = "";
        for (int i = number.length() - 1; i >= 0; i--) {
            reversed = reversed.concat(String.valueOf(number.charAt(i)));
        }

        String numberNew = Stream.of(String.valueOf(x).split(""))
                .map(StringBuilder::new)
                .map(StringBuilder::reverse)
                .collect(Collectors.joining());
        /*
        String b = String.valueOf(x);
        StringBuilder before = new StringBuilder(b);
        String after = before.reverse().toString();
        return b.equals(after);*/
        return number.equals(numberNew);
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {

        int m = nums1.length;
        int n = nums2.length;
        int finalArray[] = new int[n + m];
        int i = 0, j = 0, k = 0;
        while (i < m && j < n) {
            if (nums1[i] < nums2[j]) {
                finalArray[k++] = nums1[i++];
            } else {
                finalArray[k++] = nums2[j++];
            }
        }
        if (i < m) {
            while (i < m)
                finalArray[k++] = nums1[i++];
        }
        if (j < n) {
            while (j < n)
                finalArray[k++] = nums2[j++];
        }
        n = n + m;
        if (n % 2 == 1)
            return finalArray[((n + 1) / 2) - 1];
        else return ((double) finalArray[(n / 2) - 1] + (double) finalArray[(n / 2)]) / 2.0;
    }

    /*"babad" -> "bab" || "aba"
     * "cbbd" -> "bb"*/
    public static String longestPalindrome(String s) {
        longestPalindromeStart = 0;
        longestPalindromeLength = 0;
        int sLength = s.length();
        if (sLength < 2) {
            return s;
        }

        for (int start = 0; start < sLength - 1; start++) {
            expand(s, start, start);
            expand(s, start, start + 1);
        }

        return s.substring(longestPalindromeStart, longestPalindromeStart + longestPalindromeLength);
    }

    private static void expand(String s, int start, int end) {
        while (start >= 0 && end < s.length()
                && s.charAt(start) == s.charAt(end)) {
            start--;
            end++;
        }

        if (longestPalindromeLength < end - start - 1) {
            longestPalindromeStart = start + 1;
            longestPalindromeLength = end - start - 1;
        }

    }

    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode current = new ListNode();
        ListNode ref1 = current;
        while (list1 != null && list2 != null) {
            ListNode temp = new ListNode();
            if (list1.val < list2.val) {
                temp.val = list1.val;
                list1 = list1.next;
            } else {
                temp.val = list2.val;
                list2 = list2.next;
            }
            current = current.next = temp;
        }
        while (list1 != null) {
            current = current.next = list1;
            list1 = list1.next;
        }
        while (list2 != null) {
            current = current.next = list2;
            list2 = list2.next;
        }

        return ref1.next;
    }

    /*7*/

    public static int kaprekar(int num){
         int kaprekarCount = 0;
         return kaprekarMethod(num, kaprekarCount);
    }

    public static int kaprekarMethod(int num, int iterations) {

        if (num == 0) {
            return 0;
        } else if (num < 1000) {

            num = Integer.parseInt(String.valueOf(num)
                    .concat("0"));
        }

        int ascending = Integer.parseInt(
                Arrays.stream(String.valueOf(num)
                                .split(""))
                        .sorted()
                        .collect(Collectors.joining())
        );

        int descending = Integer.parseInt(Arrays.stream(String.valueOf(num)
                        .split(""))
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining()));

        int temp = descending - ascending;


        ++iterations;
        return temp == 6174 ? iterations : kaprekarMethod(temp,iterations);
        //return 1;
    }

}



