package com.omar.challenge;


import com.omar.model.*;
import lombok.extern.java.Log;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HackerRank {

    public static void main(String[] args) {
        maximaDiferencia(null);
    }

    /*1*/
    public static void plusMinus(List<Integer> arr) {

        int length = arr.size();
        double positives = arr.stream()
                .filter(v -> v > 0)
                .count();
        double zeros = arr.stream()
                .filter(v -> v == 0)
                .count();
        double negatives = arr.stream()
                .filter(v -> v < 0)
                .count();

        DecimalFormat decimalFormat = new DecimalFormat("#.000000");

        String positivesNumber = decimalFormat.format(positives / length);
        String negativeNumber = decimalFormat.format(negatives / length);
        String zeroNumber = decimalFormat.format(zeros / length);

        System.out.println("+:" + positivesNumber + "\n" +
                "0:" + negativeNumber + "\n" +
                "-:" + zeroNumber);

    }

    /*2*/
    public static void miniMaxSum(List<Integer> arr) {

        long maximum = arr.stream()
                .sorted(Comparator.reverseOrder())
                .limit(arr.size() - 1)
                .mapToLong(Integer::longValue)
                .reduce(Long::sum)
                .orElse(0);

        List<Integer> test = arr.stream()
                .sorted(Comparator.reverseOrder())
                .limit(4)
                .collect(Collectors.toList());

        int minimum = arr.stream()
                .sorted()
                .limit(arr.size() - 1)
                .reduce(Integer::sum)
                .orElse(0);

        System.out.println(minimum + " - " + maximum);

        String min = String.valueOf(arr.stream()
                .sorted()
                .limit(4)
                .mapToLong(Integer::longValue)
                .sum());


        String max = (String.valueOf(arr.stream()
                .sorted(Comparator.reverseOrder())
                .limit(4)
                .mapToLong(Integer::longValue)
                .sum()));


        System.out.println(min + " - " + max);
    }

    public static String timeConversion(String s) {
        //s = s.endsWith("AM") ? s.replace("AM", "PM") : s.replace("PM", "AM");
        DateFormat inDateFormat = new SimpleDateFormat("hh:mm:ssa"); ////it works
        DateFormat outDateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = null;
        try {
            date = inDateFormat.parse(s);
            outDateFormat.setTimeZone(TimeZone.getDefault());

            outDateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        String result= date.toString();
        //String result = outDateFormat.format(date);
        return result;

    }

    public static int findMedian(List<Integer> arr) {
        return arr.size() % 2 == 0 ? arr.size() + 1 : arr.size();

    }

    /*4*/
    public static int lonelyInteger(List<Integer> a) {

        int result = -1;

        a.removeIf(v -> a.stream()
                .filter(nv -> nv == v)
                .count() > 1);

        try {
            return a.get(0);
        } catch (IndexOutOfBoundsException ex) {
            return -1;
        }

        /*

        Map<Integer, Integer> occurrences = new HashMap<>();
        a.stream()
                .forEach(v -> {
                    Integer ocurrs = occurrences.get(v) != null ? occurrences.get(v) : 0;
                    ocurrs = ocurrs + 1;
                    occurrences.put(v, ocurrs);

                });

        for (Map.Entry<Integer, Integer> values : occurrences.entrySet()) {
            if (values.getValue() == 1) {
                result = values.getKey();
            }
        }
        return result;*/

    }

    public static int diagonalDifference(List<List<Integer>> arr) {
        int leftDiagonal = 0, rightDiagonal = 0;

        for (int i = 0; i < arr.size(); i++) {
            leftDiagonal += arr.get(i).get(i);
            rightDiagonal += arr.get(i).get((arr.size() - i - 1));
        }

        return Math.abs(Math.subtractExact(leftDiagonal, rightDiagonal));

    }

    public static List<Integer> countingSort(List<Integer> arr) {
        int length = arr.stream().max(Comparator.naturalOrder()).get() + 1;
        Integer[] newList = new Integer[length];

        for (int i = 0; i < arr.size(); i++) {
            int tempValue = newList[arr.get(i)] != null ? newList[arr.get(i)] + 1 : 1;
            newList[arr.get(i)] = tempValue;
        }

        return Arrays.asList(newList).stream().map(v -> v == null ? 0 : v).collect(Collectors.toList());

    }

    /*7*/
    public static void findZigZagSequence(int[] a, int n) {
        Arrays.sort(a);
        int mid = (n) / 2;
        int temp = a[mid];
        a[mid] = a[n - 1];
        a[n - 1] = temp;

        int st = mid + 1;
        int ed = n - 2;
        while (st <= ed) {
            temp = a[st];
            a[st] = a[ed];
            a[ed] = temp;
            st = st + 1;
            ed = ed - 1;
        }
        for (int i = 0; i < n; i++) {
            if (i > 0) System.out.print(" ");
            System.out.print(a[i]);
        }
        System.out.println();
    }

    /*8*/
    public static String caesarCipher(String s, int k) {
        List<String> alphabet = Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");

        return Arrays.stream(s.split(""))
                .map(character -> {
                    boolean isUpper = character.equals(character.toUpperCase());
                    int index = alphabet.indexOf(character.toLowerCase());
                    if (index < 0) {
                        return character;
                    }
                    index = (index + k) % alphabet.size();
                    return isUpper ? alphabet.get(index).toUpperCase() : alphabet.get(index);
                }).collect(Collectors.joining(""));
    }

    /*9*/
    public static String gridChallenge(List<String> grid) {
        long startTime = System.currentTimeMillis();
        String result = "YES";

        List<String> orderGrid = grid.stream()
                .map(row -> Arrays.stream(row.split(""))
                        .sorted()
                        .collect(Collectors.joining("")))
                .collect(Collectors.toList());

        for (int i = 0; i < orderGrid.get(0).length(); i++) {
            int finalI = i;
            String original = "";
            String ordered = "";
            original = orderGrid.stream()
                    .map(word -> word.charAt(finalI))
                    .map(c -> c.toString())
                    .collect(Collectors.joining(""));

            ordered = Arrays.stream(original.split(""))
                    .sorted().collect(Collectors.joining(""));

            if (!original.equals(ordered)) {
                result = "NO";
                break;
            }
        }
        Long endTime = System.currentTimeMillis() - startTime;
        System.out.println("execution time:" + endTime + "ms");
        return result;

    }

    /*10*/
    public static int superDigit(String n, int k) {
        long startTime = System.currentTimeMillis();

        String baseResult = String.valueOf(Integer.parseInt(getSuperDigit(n)) * k);
        int result = Integer.parseInt(getSuperDigit(baseResult));

        Long endTime = System.currentTimeMillis() - startTime;
        System.out.println("execution time:" + endTime + "ms");

        return result;

    }

    private static String getSuperDigit(String number) {

        if (number.length() > 1) {
            String newNum = String.valueOf(Arrays.stream(number.split(""))
                    .mapToInt(Integer::parseInt)
                    .sum());
            number = getSuperDigit(newNum);
        }
        return number;
    }

    /*11*/
    public static String minimumBribes(List<Integer> q) {
        long startTime = System.currentTimeMillis();
        int bribes = 0;
        String solution = "0";
        LinkedList<Integer> qOrdered = q.stream().sorted().collect(Collectors.toCollection(LinkedList::new));
        for (int i = 0; i < qOrdered.size(); i++) {
            Integer valueQ = q.get(i);
            Integer valueQOrdered = qOrdered.get(i);
            if (valueQ != valueQOrdered) {
                int positions = qOrdered.indexOf(valueQ) - i;
                if (positions > 2) {
                    solution = "Too chaotic";
                    break;
                }
                qOrdered.remove(valueQ);
                qOrdered.add(q.indexOf(valueQ), valueQ);
                bribes += positions;

            }
        }
        Long endTime = System.currentTimeMillis() - startTime;
        System.out.println("execution time:" + endTime + "ms");
        return solution.equals("Too chaotic") ? solution : String.valueOf(bribes);
    }

    public static SinglyLinkedListNode mergeLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {

        SinglyLinkedListNode emptyNode = new SinglyLinkedListNode(0, null);
        SinglyLinkedListNode result = new SinglyLinkedListNode(0, null);
        SinglyLinkedListNode  iterator = result.next;

        int carry=0;
        while (head1 != emptyNode && head2 != emptyNode) {
            int sum = head1.data + head2.data + carry;
            carry = sum % 10;
            int value = sum / 10;

            iterator = iterator.next = new SinglyLinkedListNode(value,null);
            head1 = head1.next == null ? emptyNode : head1.next;
            head2 = head2.next == null ? emptyNode : head2.next;

        }

        while (head1.next != null){
            //iterator = iterator.next
        }

        return result.next;
    }

    public void test() {
        Car wagonR = new WagonR(5);
    }

    public static String typeCounter(String sentence) {

        Long strings = Arrays.stream(sentence.split(" "))
                .filter(x -> x.matches("([A-Z]+)|([a-z]+)"))
                .count();

        Long longs = Arrays.stream(sentence.split(" "))
                .filter(x -> x.matches("\\d*\\.\\d+"))
                .count();

        Long integers = Arrays.stream(sentence.split(" "))
                .filter(x -> x.matches("\\d+^."))
                .count();

        System.out.println(strings);
        System.out.println(longs);
        System.out.println(integers);

        return "";

    }

    public void regexRepeatWord(String input) {
        String regex = "";
        Pattern p = Pattern.compile(regex, Pattern.LITERAL);

        Matcher m = p.matcher(input);

        // Check for subsequences of input that match the compiled pattern
        while (m.find()) {
            //input = input.replaceAll("\\d");
        }

        // Prints the modified sentence.
        System.out.println(input);
    }

    //15-3-6-10
    public static int maximaDiferencia(List<Integer> a) {

        int max = -1;
        for (int i = a.size() - 1; i >= 0; i--) {
            int position = i;
            int valueI = a.get(i);
            while (position >= 1) {
                int tempResult = valueI - a.get(position - 1);
                tempResult = tempResult >= 0 ? tempResult : -1;
                max = Math.max(tempResult, max);
                position--;

            }
        }
        //System.out.println(max);
        return max;
    }


}


