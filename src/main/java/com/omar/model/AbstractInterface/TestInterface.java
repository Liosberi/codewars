package com.omar.model.AbstractInterface;

interface Interface {

    /*by default are public static final*/
    int interfaceAge = 0;

        /* doesn't work
        int getAge(){
            return age;
        }
         */

    /*this one is public abstract by default*/
    void interfaceAbstractMethod();

    /*default method, not obliged to override*/
    default void defaultMethod() {
        System.out.println("defaultMethod, not needed override, but you can");
    }

    /*static method, not obliged to override*/
    static void staticMethod() {
        System.out.println("staticMethod, cannot be overwritten and does not even allow");
    }

}


public class TestInterface implements Interface {

    public static void main(String[] args) {

                   /*doesn't allow cause is final
            Interface.interfaceAge=10;
                    */
        int staticInt = Interface.interfaceAge;
        System.out.println("public static final field: "+staticInt);
        Interface.staticMethod();

        Interface ii = new Interface() {
            @Override
            public void interfaceAbstractMethod() {

            }
        };

        Interface interfaceInstance = new TestInterface();
        interfaceInstance.interfaceAbstractMethod();
        interfaceInstance.defaultMethod();
    }

    @Override
    public void interfaceAbstractMethod() {
        System.out.println("implementing the interface abstract method");
    }

}