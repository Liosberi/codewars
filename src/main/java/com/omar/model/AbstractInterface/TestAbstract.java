package com.omar.model.AbstractInterface;

abstract class Abstract {
    /*you can't instantiate an abstract class???
    * maybe it makes reference to normal classes more than interface comparison*/

    protected int abstractAge = 0;

    static final int abstractNumber=1;

    protected abstract void abstract_abstractMethod();

    public void getAge(){
        System.out.println("Abstract fields:\n"+"protected: "+abstractAge+"\nstatic final: "+abstractNumber);
    }

}

public class TestAbstract extends Abstract{

    public static void main(String[] args) {

        Abstract abstractInstance= new TestAbstract();
        abstractInstance.abstractAge=20;
        abstractInstance.abstract_abstractMethod();
        abstractInstance.getAge();

        Abstract a = new Abstract() {
            @Override
            protected void abstract_abstractMethod() {

            }
        };

        Abstract abstractInstanceB= new TestAbstract();
        abstractInstanceB.getAge();

        System.out.println(Abstract.abstractNumber);

    }


    @Override
    protected void abstract_abstractMethod() {
        System.out.println("implementing the Abstract abstract method");
    }
}