package com.omar.model;

public final class ImmutableClass implements Cloneable{

    /*class is final to avoid be extended*/
    private final String name;
    private final int age;
    private final Address address;

    /*fields are private to deny access to them
    * values are initialized on constructor*/


    public ImmutableClass(String name, int age, Address address) {
        this.name = name;
        this.age = age;
        Address newAddress;
        //here you should use deep copy with cloneable if the clas is not final
        newAddress =  new Address(address.getType(),address.getBlock());
        this.address = newAddress;
    }


    /*with object fields be careful to not return reference,
    * create a copy instead*/

    public Address getAddress() {
        /* wrong: return address;*/
        return new Address(address.getType(), address.getBlock());
    }

    public class test {

        ImmutableClass immutableClass= new ImmutableClass("Omar", 26, null);
        Address address= new Address();
    }

}
