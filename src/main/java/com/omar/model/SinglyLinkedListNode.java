package com.omar.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
public class SinglyLinkedListNode {
    public int data;
    public SinglyLinkedListNode next;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SinglyLinkedListNode o1 = (SinglyLinkedListNode) o;
        return data == o1.data && Objects.equals(this.next, o1.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, next);
    }
}
