package com.omar.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@NoArgsConstructor
@Data
public class User {

    private String name;
    private int age;
    private Address address;
    private List<Address> addresses;

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void getUsername(){
        System.out.println("father");
    }

}
