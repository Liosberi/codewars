package com.omar.model;

public class WagonR extends Car {
    private int carMileage;

    public WagonR(int carMileage) {
        super(false, "4");
        this.carMileage = carMileage;
    }

    @Override
    public String getMileage() {
        return String.valueOf(carMileage);
    }
}
