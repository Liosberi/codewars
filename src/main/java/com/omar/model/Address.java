package com.omar.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Address implements Cloneable {

    private String type;
    private String block;

    public Address(String type, String block) {
        this.type = type;
        this.block = block;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Address newAddress = (Address) super.clone();
        newAddress.setBlock(this.getBlock());
        newAddress.setType(this.getType());
        return newAddress;

        /*all above suppose to be no necessary cause the field are normal (no objets)
        //return super.clone();
        * */
    }
}
