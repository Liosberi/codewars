package com.omar.model;

import java.util.Objects;

public class Modifier {

    public final  String field5="";

    private String field1;

    private static String field2;

    public Modifier() {
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public static String getField2() {
        return field2;
    }

    public static void setField2(String field2) {
        Modifier.field2 = field2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Modifier modifier = (Modifier) o;
        return Objects.equals(field1, modifier.field1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field1);
    }

    public final class SubModifier{

        protected String field3;
        public String field4;

        public void subModifierMethod1(){}
    }
}
