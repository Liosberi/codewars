package com.omar.model;

import lombok.Builder;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@ToString
@SuperBuilder(toBuilder = true)
public class BuildClass {

    private String a;
    private String b;
}
