package com.omar.lab;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;
import com.omar.model.*;
import lombok.var;

public class Lab {

    final static Logger LOGGER = Logger.getLogger(Lab.class.toString());

    public static void main(String[] args) throws CloneNotSupportedException, InterruptedException, ExecutionException {
        testCompletableFuture();

        //validatePrint();
/*
        maps();

        playFatherSon();
        playBuilders();


        Lab labb = new Lab();
        labb.getTimes();

        setAndArray();
        largestNumber(new int[]{60, 530, 50, 5000, 5400, 750});
        playWithImmutableClass();
        getParameter();*/

        //histoGrama();

        //playingWithMaps();

        //numbersRepeated();

        //employeesBySalary();

       /* System.out.println(calPoints(new String[]{"5", "2", "C", "D", "+"}));
        System.out.println(calPoints(new String[]{"5", "-2", "4", "C", "D", "9", "+", "+"}));*/

        /*System.out.println("{} is: " + isParenthesesValid("{}"));
        System.out.println("()[]{} is: " + isParenthesesValid("()[]{}"));
        System.out.println("(} is: " + isParenthesesValid("(}"));
        System.out.println("({)} is: " + isParenthesesValid("({)}"));
        System.out.println("({}) is: " + isParenthesesValid("({})"));*/

        //printArray();

    }

    public static long getParameter() {
        User omar = new User();
        omar.setName("");
        omar.setAge(1);

        long num = 1402933;
        long min = 1000;
        double percentage = Optional.ofNullable(omar)
                .map(User::getName)
                //.map(Lab::getLong)
                .filter(l -> !l.isEmpty())
                .map(Double::valueOf)
                //.orElseGet(NumberUtils.DOUBLE_ONE::doubleValue);
                .orElseGet(() -> 2d);

        System.out.println(percentage);

        VipUser vipUser = new VipUser();

        return num * percentage > min ? min : (long) percentage;
    }

    public static String getLong(String s) {
        return "0.0003";
    }

    public static void usePredicate() {
        Predicate<String> containValue = s -> ImmutableList.of("A", "B").contains(s);

        Stream.of("omar", "B", null)
                .filter(containValue)
                .forEach(System.out::print);
        ;
    }

    public void checkingCollections() {
        Collection<String> collection = new ArrayList<>();
        //collection.ge
        collection.contains("");
        List<String> list = new ArrayList<>();
        list.get(1);
    }

    public static void cachingErrorResponse() {
        LOGGER.info("weStart");
        User omar = new User("omar", 26);
        omar.setAddress(new Address("cause wrong enter type", "2"));
        String errorMessage = Optional.ofNullable(omar)
                .map(User::getAddress)
                .map(address -> address.getType().concat(" " + address.getBlock()))
                .orElse("");

        LOGGER.warning("something happens: " + errorMessage);
    }

    public static void checkingBufferIncrementPosition() {
        int[] buffer = new int[10];
        int counter = 0;

        for (int i = 1; i < buffer.length; i++) {
            int tempt = buffer[counter];
            int value = counter;
            buffer[counter++] = 1;
        }

    }

    public static void checkEmptyStream() {
        User omar = new User("omar", 26);
        //omar.setAddresses(ImmutableList.of(new Address("House","91"),new Address("Apartment","203")));
        List<Address> listAddres = Optional.ofNullable(omar)
                .map(User::getAddresses)
                .map(List::stream)
                .orElseGet(Stream::empty)
                .collect(Collectors.toList());
        System.out.println(listAddres);

        Address testAddress = new Address();
        //Address.SubAddress subAddress = testAddress.new SubAddress("test");

        //Address.SubAddress subAddress= new testAddress.SubAddress("test");

    }

    public void playWithModifier() {

        Modifier modifier1;
        modifier1 = new Modifier();
        modifier1.getField1();
        //modifier1.field5=""; /*it can't be*/


        Modifier.SubModifier subModifier1 = modifier1.new SubModifier();
        subModifier1.field4 = "";
        subModifier1.subModifierMethod1();

        Modifier.getField2();

    }

    public static void playWithImmutableClass() throws CloneNotSupportedException {
        Address address1 = new Address("Apartment", "1");
        Address address2 = (Address) address1.clone();
        address2.setType("House");

        System.out.println(address2.getType() + "addres1:" + address1.getType());

        Set<String> setColl = new HashSet<>();
        setColl.add("a");
        setColl.stream().sorted();

    }


    /*
    505 765 763
    60 530 50  5000 5400 750 -> 750605400530500050 -> 76555
    530 50 5000 5400 --> 53 5 5 54 -> 54 53 5(1) 5(3)
    5400 530 50 5000
*/
    public static void largestNumber(int[] nums) {

        Comparator<String> largesComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String obj1 = o1.concat(o2);
                String obj2 = o2.concat(o1);
                return obj2.compareTo(obj1);
            }
        };

        String result = Arrays.stream(nums)
                .mapToObj(String::valueOf)
                .sorted(largesComparator)
                .collect(Collectors.joining());

        System.out.println(result);
    }

    public static void setAndArray() {

        Object[] arrayOB = new Object[3];
        arrayOB[0] = new Address();
        arrayOB[1] = new ListNode();
        arrayOB[2] = null;

        Set<Integer> set = new HashSet<>();
        Integer[] array = new Integer[3];

        set.add(1);
        set.add(2);
        set.add(3);


        array[0] = 1;
        array[1] = 2;
        array[2] = 3;


        set.stream().forEach(System.out::println);
        Arrays.stream(array).forEach(System.out::println);
        Arrays.stream(arrayOB).forEach(System.out::println);
    }

    public static void arrayListAndLinkedList() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();


        Integer ob = 1;
        arrayList.add(1);
        arrayList.add(2);


        linkedList.add(1);
        linkedList.add(2);

        arrayList.remove(1);
        linkedList.remove(1);
        linkedList.remove(ob);

        Iterator<Integer> linkedListIterator = linkedList.listIterator();
        while (linkedListIterator.hasNext()) {

            Integer value = linkedListIterator.next();
            if (value > 0) {
                linkedListIterator.remove();
            }
        }

        Iterator<Integer> arrayListIterator = arrayList.listIterator();
        while (arrayListIterator.hasNext()) {
            Integer value = arrayListIterator.next();
            if (value > 0) {
                arrayListIterator.remove();

            }
        }

    }

    public static void maps() {
        Map<String, Integer> map = new HashMap<>();

        map.put(null, null);
        map.put(null, 3);
        map.put(null, null);
        map.put(null, 4);

        System.out.println(map);
        WeakHashMap<String, String> weakHashMap;
    }

    public void getTimes() {
        TimeProvider timeProvider = new TimeProvider();

        System.out.println(timeProvider.getMillis());
        //Getting the current date
        Date date = new Date();
        //This method returns the time in millis
        long timeMilli = date.getTime();
        System.out.println("Time in milliseconds using Date class: " + timeMilli);

        //creating Calendar instance
        Calendar calendar = Calendar.getInstance();
        //Returns current time in millis
        long timeMilli2 = calendar.getTimeInMillis();
        System.out.println("Time in milliseconds using Calendar: " + timeMilli2);

        //Java 8 - toEpochMilli() method of ZonedDateTime
        System.out.println("Getting time in milliseconds in Java 8: " +
                ZonedDateTime.now().toInstant().toEpochMilli());


        System.out.println(timeProvider.getMillis());
        System.out.println(timeProvider.getMillis());
        System.out.println(timeProvider.getMillis());
        System.out.println(timeProvider.getMillis());
    }

    class TimeProvider {
        long getMillis() {
            Date date = new Date();
            return date.getTime();
        }
    }

    public static void playBuilders() {

        BuildClass build1 = BuildClass.builder()
                .a("tengo a")
                .build();

        BuildClass build2 = build1.toBuilder()
                .b("tengo b")
                .build();

        System.out.println("build1: " + build1);
        System.out.println("build2: " + build2);

        StringBuilder stringBuilder1 = new StringBuilder("Omar");
        stringBuilder1.append("david");
        stringBuilder1.append("sanchez");

        StringBuilder stringBuilder2 = stringBuilder1.append("Ortiz");

        System.out.println("stringBuilder1: " + stringBuilder1);
        System.out.println("stringBuilder2: " + stringBuilder2);
    }

    public static void playFatherSon() {

        User father = new User();
        VipUser son = new VipUser();

        father.getUsername();
        son.getUsername();

    }

    public static void playSet() {
        Set<String> newSet = new HashSet<>();
        newSet.add("omar");

        System.out.println(newSet);
    }

    public static String isSymmetric() {
        String[] myArray = {"a", "b", "c", "d", "d", "c", "b", "a"};
        int n = 8;
        int mid = (myArray.length / 2) - 1;
        String result = "Symmetric";
        for (int i = 0; i <= mid; i++) {
            int end = myArray.length - (i + 1);

            String beginNumber = myArray[i];
            String endNumber = myArray[end];

            if (!beginNumber.equals(endNumber)) {
                result = "Asymmetric";
                break;
            }
        }

        return result;
    }

    public static void histoGrama() {
        int[] myArray = {1, 2, 1, 3, 3, 1, 2, 1, 5, 1};
        Map<Integer, String> mapHist = new HashMap<>();

        for (int i = 0; i < myArray.length; i++) {
            int tempKey = myArray[i];
            String counter = "*";
            if (mapHist.containsKey(tempKey)) {
                String tempVal = mapHist.get(tempKey);
                tempVal = tempVal.concat(counter);
                mapHist.put(tempKey, tempVal);
            } else {
                mapHist.put(tempKey, counter);
            }
        }
        int numbers = 1;
        while (numbers <= 5) {
            String count = null == mapHist.get(numbers) ? "" : mapHist.get(numbers);
            System.out.println(numbers + ": " + count);
            numbers++;
        }
    }

    /*validate maps are not null nor empty,
    key are not empty,
    values are not empty,
    and convert the mapas into Long, long
    * */
    /*Map<Long, Long> count(Map<String, UserStats>... visits) {
        Predicate<Map<String, UserStats>> mapIsNotNull = m -> Objects.nonNull(m);
        Predicate<Map<String, UserStats>> mapIsNotEmpty = m -> !m.isEmpty();
        //Predicate<Set<Map.Entry<String, UserStats>>> valesNotEmpty = e -> e. ;
        Predicate<Map<String, UserStats>> isValidMap = mapIsNotNull.and(mapIsNotEmpty);

        var a = Arrays.stream(visits)
                .filter(isValidMap)
                .flatMap(m -> m.entrySet().stream()
                        .filter(e -> Objects.nonNull(e.getValue())))
                .map(entry -> entry.setValue(entry.getValue().getVisitCount()))
                .count();
        ;

        return null;
    }*/


    /*Find the number of times that each character comes in the string
     *fin the first not repeated element in the stinrg */
    public static void playingWithMaps() {
        String word = "OmarDavidSanchezOrtiz";

        Map<Character, Long> result1 = word.chars()
                .mapToObj(letter -> Character.toLowerCase(Character.valueOf((char) letter)))
                .collect(Collectors.groupingBy(
                        Function.identity(), Collectors.counting()
                ));

        Character result2 = word.chars()
                .mapToObj(letter -> Character.toLowerCase(Character.valueOf((char) letter)))
                .collect(Collectors.groupingBy(
                        Function.identity(), Collectors.counting()
                ))
                .entrySet()
                .stream().filter(e -> e.getValue() == 1)
                .findFirst()
                .map(Map.Entry::getKey)
                .get();

        System.out.println(result1);
        System.out.println(result2);

    }

    /*Count number of times that each number is repeated
     * */
    public static void numbersRepeated() {
        List<Integer> numList = Arrays.asList(2, 3, 4, 2, 1, 2, 3, 43, 2);
        Map<Integer, Long> result = numList.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(result);

    }

    /*order employees by salary*/
    public static void employeesBySalary() {
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee(1, "A", 500000));
        employeeList.add(new Employee(2, "B", 5000));
        employeeList.add(new Employee(3, "C", 123));
        employeeList.add(new Employee(4, "D", 673));
        employeeList.add(new Employee(5, "E", 510000));
        employeeList.add(new Employee(6, "F", 9999));

        Comparator<Employee> bySalary = (a, b) -> b.getSalary() - a.getSalary();
        Comparator<Employee> byLargeSalary = (a, b) -> String.valueOf(a.getSalary()).compareTo(String.valueOf(b.getSalary()));

        List<Employee> result = employeeList.stream()
                .sorted(bySalary)
                .collect(Collectors.toList());

        Employee result2 = employeeList.stream()
                .sorted(bySalary)
                .skip(2)
                .findAny().get();


        System.out.println(result);
        System.out.println(result2);

    }

    public static int calPoints(String[] ops) {
        Predicate<String> isNumber = s -> s.matches("^-?[0-9]+$");

        Map<String, Consumer<List<Integer>>> performOperation =
                Map.of("+", record -> record.add(record.get(record.size() - 1) + record.get(record.size() - 2)),
                        "D", record -> record.add(record.get(record.size() - 1) * 2),
                        "C", record -> record.remove(record.size() - 1));

        Function<String, Consumer<List<Integer>>> performAction = s -> isNumber.test(s) ? record -> record.add(Integer.parseInt(s)) : performOperation.get(s);

        List<Integer> record = new ArrayList<>();

        Arrays.stream(ops)
                .map(performAction::apply)
                .forEach(c -> c.accept(record));

        return record.stream().collect(Collectors.summingInt(Integer::intValue));
    }

    public static String isParenthesesValid(String s) {
        //({)}

        Map<String, String> brackets = Map.of("(", ")", "{", "}", "[", "]");
        Stack<String> openBrackets = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            String temp = String.valueOf(s.charAt(i));
            if (brackets.containsKey(temp)) {
                openBrackets.push(brackets.get(temp));
            } else {
                try {
                    String getHead = openBrackets.pop();
                    if (!getHead.equals(temp)) {
                        return "unbalanced";
                    }
                } catch (EmptyStackException e) {
                    return "unbalanced";
                }
            }
        }
        if (openBrackets.isEmpty()) {
            return "balanced";
        } else {
            return "unbalanced";
        }
    }

    public static void printArray(){
        Employee omar = new Employee(1,"omar",1);
        Employee omar2 = new Employee(1,"omar",1);
        Employee[] arrayEm = new Employee[]{omar2,omar,omar2};

        System.out.println(arrayEm);
        System.out.println(Arrays.toString(arrayEm));

        Arrays.stream(arrayEm).forEach(System.out::println);

        for (Employee a:arrayEm) {
            System.out.println(a);
        }

    }


    public static void validatePrint(){
        List<Address> addressesList = new ArrayList<>();
        Address addressUser = new Address("type", "block");
        addressesList.add(addressUser);
        addressesList.add(addressUser);

        User omar= new User();
        omar.setAddresses(addressesList);

        User empty = new User();
        User nullUser = null;

        String validated= Optional.ofNullable(omar)
                .map(User::getAddresses)
                .flatMap(addresses ->
                {
                    for (Address address:addresses) {
                        System.out.println(address.getType());
                    }
                    return Optional.ofNullable("validated");
                }).orElse("validated");

        System.out.println(validated);

    }

    public static void testCompletableFuture() throws InterruptedException, ExecutionException {

        System.out.println("thread from main: "+Thread.currentThread().getName());

        Runnable runnableTask = () -> System.out.println("runnable Task: "+Thread.currentThread().getName());
        Runnable runnableTaskDelay = () -> {
            try {
                Thread.sleep(10000);
                System.out.println("runnable delay Task: "+Thread.currentThread().getName());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
        Callable<String> callableTask = () -> "callableTask Task: "+Thread.currentThread().getName();
        Supplier<String> supplierTask = () -> "supplierTask Task: "+ Thread.currentThread().getName();

        ExecutorService executor = Executors.newSingleThreadExecutor();

/*
        Future futureRes = executor.submit(runnableTask);
        Future<String> futureRes2 = executor.submit(callableTask);*/

        //CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(supplierTask);
       //CompletableFuture<String> completableFuture2 = CompletableFuture.supplyAsync(supplierTask,executor);
       CompletableFuture<Void> completableFuture3 = CompletableFuture.runAsync(runnableTask, executor);
       /*this one is executed in the ForkJoinPool.commonPool-worker- that has thread by demand and don't prevent the jvm from exit before end the task*/
        CompletableFuture<Void> completableFuture4 = CompletableFuture.runAsync(runnableTaskDelay);



        //Thread.sleep(1000);
        executor.shutdown();

    }

}
