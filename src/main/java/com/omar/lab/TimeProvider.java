package com.omar.lab;


import java.util.*;
import java.util.function.Predicate;

//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

interface TimeProvider {
    long getMillis();
}

class TimeproviderImpl implements TimeProvider {

    @Override
    public long getMillis() {
        Date date = new Date();
        return date.getTime();
    }
}

class CachingDataStructure {

    private int maxSize;
    private TimeProvider timeProvider;
    Map<String, DataValue> structure;
    private final Predicate<DataValue> goodValues = v -> v.getTimeToLeaveInMilliseconds() > timeProvider.getMillis();

    CachingDataStructure(TimeProvider timeProvider, int maxSize) {
        this.timeProvider = timeProvider;
        this.maxSize = maxSize;
        structure = new HashMap<>(maxSize);
    }

    public void put(String key, String value, long timeToLeaveInMilliseconds) {
        if (Objects.nonNull(key) && Objects.nonNull(value) && Objects.nonNull(timeToLeaveInMilliseconds)) {
            throw new IllegalArgumentException("invalid parameters");
        }
        long currentTune = timeProvider.getMillis();
        DataValue newValue = new DataValue(value, currentTune + timeToLeaveInMilliseconds);
        if (!isFull()) {
            structure.put(key, newValue);
        } else {
            if (existKey(key)) {
                DataValue oldValue = structure.get(key);
                if (newValue.getTimeToLeaveInMilliseconds() > oldValue.getTimeToLeaveInMilliseconds()) {
                    structure.put(key, newValue);
                }
            }
        }


    }

    public Optional<String> get(String key) {
        return Optional.ofNullable(structure.get(key))
                .filter(goodValues)
                .map(DataValue::getValue);
    }

    boolean existKey(String key) {
        return structure.containsKey(key);
    }

    boolean isFull() {
        return maxSize >= size();
    }

    public int size() {

        //mirar si expiro para la cuenta
        return (int) structure.entrySet().stream()
                .map(Map.Entry::getValue)
                .filter(goodValues)
                .count();
    }

}


class DataValue {

    private String value;
    private Long timeToLeaveInMilliseconds;

    public DataValue(String value, Long timeToLeaveInMilliseconds) {
        this.value = value;
        this.timeToLeaveInMilliseconds = timeToLeaveInMilliseconds;
    }

    public String getValue() {
        return value;
    }

    public Long getTimeToLeaveInMilliseconds() {
        return timeToLeaveInMilliseconds;
    }
}


