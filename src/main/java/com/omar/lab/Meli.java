package com.omar.lab;

import java.util.ArrayList;
import java.util.List;

public class Meli {

    public static void main(String[] args) {
        boolean result=test(new int[]{1,3,7},8 );

        System.out.println(result);

        boolean result2=test(new int[]{4,3,7,4},8 );

        System.out.println(result2);
    }

    public static boolean test(int[] integers, int k){
        boolean result= false;
        List<Integer> recorriendo= new ArrayList<>();
        for (int i=0; i<integers.length;i++){
            int temp= integers[i];
            int expected= k-temp;
            if(recorriendo.contains(expected)){
                return true;
            }
            else {
             recorriendo.add(temp);
            }
        }

        return result;
    }
}
