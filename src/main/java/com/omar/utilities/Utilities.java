package com.omar.utilities;

import java.math.BigInteger;

public class Utilities {
    public static BigInteger calculateFactorial(BigInteger n) {
        return n.equals(BigInteger.ONE) ? BigInteger.ONE : n.multiply(calculateFactorial(n.subtract(BigInteger.ONE)));
    }
}
