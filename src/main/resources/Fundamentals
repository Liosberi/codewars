1. Array.dif
    Your goal in this kata is to implement a difference function, which subtracts one list from another and returns the result.
    It should remove all values from list a, which are present in list b keeping their order.
    Kata.arrayDiff(new int[] {1, 2}, new int[] {1}) => new int[] {2}
    If a value is present in b, all of its occurrences must be removed from the other:
    Kata.arrayDiff(new int[] {1, 2, 2, 2, 3}, new int[] {2}) => new int[] {1, 3}

2. Convert boolean values to strings 'Yes' or 'No'.
    Complete the method that takes a boolean value and return a "Yes" string for true, or a "No" string for false.

3. Isograms
    An isogram is a word that has no repeating letters, consecutive or non-consecutive. Implement a function that determines whether a string that contains only letters is an isogram.
     Assume the empty string is an isogram. Ignore letter case.
   Example: (Input --> Output)
   "Dermatoglyphics" --> true
    "aba" --> false
   "moOse" --> false (ignore letter case)

   str.length() == str.toLowerCase().chars().distinct().count();
   str.toLowerCase()
                     .chars()
                     .distinct()
                     .count() == str.length();
     Set<Character> letters = new HashSet<Character>();

        for (int i = 0; i < str.length(); ++i) {
          if (letters.contains(str.toLowerCase().charAt(i))) {
            return false;
          }

          letters.add(str.charAt(i));
        }

        return true;

4. It's pretty straightforward. Your goal is to create a function that removes the first and last characters of a string. You're given one parameter,
   he original string. You don't have to worry with strings with less than two characters.

5. Descending Order: Your task is to make a function that can take any non-negative integer as an argument and return it with its digits in descending order.
   Essentially, rearrange the digits to create the highest possible number.
   Examples:
   Input: 42145 Output: 54421
   Input: 145263 Output: 654321
   Input: 123456789 Output: 987654321

6. StringEndsWith
   Complete the solution so that it returns true if the first argument(string) passed in ends with the 2nd argument (also a string).
      Examples:
   solution('abc', 'bc') // returns true
   solution('abc', 'd') // returns false

7.  Odd or Even
    Given a list of integers, determine whether the sum of its elements is odd or even.
    Give your answer as a string matching "odd" or "even".
    If the input array is empty consider it as: [0] (array with a zero).
    Examples:
    Input: [0]
    Output: "even";
    Input: [0, 1, 4]
    Output: "odd";
    Input: [0, -1, -5]
    Output: "even";

8.  Reverse words
    Complete the function that accepts a string parameter, and reverses each word in the string. All spaces in the string should be retained.
    Examples
    "This is an example!" ==> "sihT si na !elpmaxe"
    "double  spaces"      ==> "elbuod  secaps"

9. Sum between numbers
    Given two integers a and b, which can be positive or negative, find the sum of all the integers between and including them and return it. If the two numbers are equal return a or b.
    Note: a and b are not ordered!
    Examples (a, b) --> output (explanation)
    (1, 0) --> 1 (1 + 0 = 1)
    (1, 2) --> 3 (1 + 2 = 3)
    (0, 1) --> 1 (0 + 1 = 1)
    (1, 1) --> 1 (1 since both are same)
    (-1, 0) --> -1 (-1 + 0 = -1)
    (-1, 2) --> 2 (-1 + 0 + 1 + 2 = 2)

10. Keep Hydrated!
    Nathan loves cycling.
    Because Nathan knows it is important to stay hydrated, he drinks 0.5 litres of water per hour of cycling.
    You get given the time in hours and you need to return the number of litres Nathan will drink, rounded to the smallest value.
    For example:
    time = 3 ----> litres = 1
    time = 6.7---> litres = 3
    time = 11.8--> litres = 5

11. Count the smiley faces!
    Given an array (arr) as an argument complete the function countSmileys that should return the total number of smiling faces.

    Rules for a smiling face:

    Each smiley face must contain a valid pair of eyes. Eyes can be marked as : or ;
    A smiley face can have a nose but it does not have to. Valid characters for a nose are - or ~
    Every smiling face must have a smiling mouth that should be marked with either ) or D
    No additional characters are allowed except for those mentioned.

    Valid smiley face examples: :) :D ;-D :~)
    Invalid smiley faces: ;( :> :} :]

    Example
    countSmileys([':)', ';(', ';}', ':-D']);       // should return 2;
    countSmileys([';D', ':-(', ':-)', ';~)']);     // should return 3;
    countSmileys([';]', ':[', ';*', ':$', ';-D']); // should return 1;
    Note
    In case of an empty array return 0. You will not be tested with invalid input (input will always be an array). Order of the face (eyes, nose, mouth) elements will always be the same.