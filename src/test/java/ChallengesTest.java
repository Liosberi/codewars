import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import com.omar.challenge.Challenges;
import com.omar.model.ListNode;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ChallengesTest {

    @Test
    public void testTrailingZeros_1() {
        assertThat(Challenges.trailingZeros(0), is(0));
        assertThat(Challenges.trailingZeros(6), is(1));
        assertThat(Challenges.trailingZeros(14), is(2));
    }

    @Test
    public void testPrime_2() {
        assertFalse("8 is not prime", Challenges.isPrime(8));
        assertFalse("9 is not prime", Challenges.isPrime(9));
        assertFalse("45 is not prime", Challenges.isPrime(45));
        assertFalse("-5 is not prime", Challenges.isPrime(-5));

        assertTrue("3 is prime", Challenges.isPrime(3));
        assertTrue("5 is prime", Challenges.isPrime(5));
        assertTrue("7 is prime", Challenges.isPrime(7));
        assertTrue("41 is prime", Challenges.isPrime(41));
        assertTrue("5099 is prime", Challenges.isPrime(5099));

        assertFalse("0  is not prime", Challenges.isPrime(0));
        assertFalse("1  is not prime", Challenges.isPrime(1));
        assertTrue("2  is prime", Challenges.isPrime(2));
        assertTrue("73 is prime", Challenges.isPrime(73));
        assertFalse("75 is not prime", Challenges.isPrime(75));
        assertFalse("-1 is not prime", Challenges.isPrime(-1));
    }

    @Test
    public void testBalance_3() {
        assertThat(Challenges.getBalance(new int[]{100, 100, 100, -10}, new String[]{"2020-12-31", "2020-12-22", "2020-12-03", "2020-12-29"}), is(230));
        assertThat(Challenges.getBalance(new int[]{180, -50, -25, -25}, new String[]{"2020-01-01", "2020-01-01", "2020-01-01", "2020-01-31"}), is(25));
        assertThat(Challenges.getBalance(new int[]{1, -1, 0, -105, 1}, new String[]{"2020-12-31", "2020-04-04", "2020-04-04", "2020-04-14", "2020-07-12"}), is(-164));
        assertThat(Challenges.getBalance(new int[]{100, 100, -10, -20, -30}, new String[]{"2020-01-01", "2020-02-01", "2020-02-11", "2020-02-05", "2020-02-08"}), is(80));
    }


    @Test
    public void testCamelCaseWithUnder_4() {
        assertEquals("(123) 456-7890", Challenges.createPhoneNumber(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}));
    }

    @Test
    public void testSequence_5() throws Exception {
        assertEquals("Empty arrays should have a max of 0", 0, Challenges.sequence(new int[]{}));
        assertEquals("Example array should have a max of 6", 6, Challenges.sequence(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }

    @Test
    public void testSinglePermutation_6() {
        assertEquals(new ArrayList<>(Arrays.asList("ab", "ba")),
                Challenges.singlePermutations("ab").stream().sorted().collect(Collectors.toList()));
        assertEquals(new ArrayList<>(Arrays.asList("a")),
                Challenges.singlePermutations("a").stream().sorted().collect(Collectors.toList()));
        assertEquals(new ArrayList<>(Arrays.asList("aabb", "abab", "abba", "baab", "baba", "bbaa")),
                Challenges.singlePermutations("aabb").stream().sorted().collect(Collectors.toList()));
        assertEquals(new ArrayList<>(Arrays.asList("abcd", "abdc", "acbd", "acdb", "adbc", "adcb",
                        "bacd", "badc", "bcad", "bcda", "bdac", "bdca",
                        "cabd", "cadb", "cbad", "cbda", "cdab", "cdba",
                        "dabc", "dacb", "dbac", "dbca", "dcab", "dcba")),
                Challenges.singlePermutations("abcd").stream().sorted().collect(Collectors.toList()));
    }


    @Test
    public void testTwoSum() {
        assertArrayEquals(new int[]{0, 1}, Challenges.twoSum(new int[]{2, 7, 11, 15}, 9));
        assertArrayEquals(new int[]{-1, 1}, Challenges.twoSum(new int[]{2, 7, 11, 15}, 7));
    }

    @Test
    public void romanToInt() {
        assertThat(Challenges.romanToInt("LVIII"), is(58));
        assertThat(Challenges.romanToInt("MCMXCIV"), is(1994));
        assertThat(Challenges.romanToInt("D"), is(500));
        assertThat(Challenges.romanToInt("I"), is(1));
    }

    @Test
    public void fizzBuzz() {
        assertThat(
                Challenges.fizzBuzz(15), is(new ArrayList<>(Arrays.asList("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"))));
    }

    @Test
    public void addTwoNumbers() {
        ListNode listNode1 = new ListNode(2, new ListNode(4, new ListNode(3, new ListNode(3, new ListNode(3)))));
        ListNode listNode2 = new ListNode(5, new ListNode(6, new ListNode(4)));
        ListNode listNode3 = new ListNode(7, new ListNode(0, new ListNode(8, new ListNode(3, new ListNode(3)))));
        assertThat(Challenges.addTwoNumbers(listNode1, listNode2), is(listNode3));
        //assertThat(Challenges.addTwoNumbers(listNode1, listNode2), is(listNode2));
    }

    @Test
    public void testLengthOfLongestSubstring() {
        assertThat(Challenges.lengthOfLongestSubstring("dvdf"), is(3));
        assertThat(Challenges.lengthOfLongestSubstring("abcabcbb"), is(3));
        assertThat(Challenges.lengthOfLongestSubstring("bbbbb"), is(1));
        assertThat(Challenges.lengthOfLongestSubstring("pwwkew"), is(3));
    }

    @Test
    public void testIsPalindrome() {
        assertTrue(Challenges.isPalindrome(1221));
    }

    @Test
    public void testMedianTwoSortedArrays() {
        assertThat(Challenges.findMedianSortedArrays(new int[]{3, 4, 5, 6}, new int[]{1, 7}), is(4.5d));
        assertThat(Challenges.findMedianSortedArrays(new int[]{3, 4}, new int[]{}), is(3.5d));
        assertThat(Challenges.findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4}), is(2.5d));
        assertThat(Challenges.findMedianSortedArrays(new int[]{3, 4}, new int[]{1, 2}), is(2.5d));
        assertThat(Challenges.findMedianSortedArrays(new int[]{1, 2}, new int[]{2}), is(2d));

    }

    @Test
    public void testLongestPalindrome() {
        assertThat(Challenges.longestPalindrome("cbbd"), is("bb"));
        assertThat(Challenges.longestPalindrome("abcdeff"), is("ff"));
    }

    @Test
    public void testMergeTwoLists() {
        ListNode listNode1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        ListNode listNode2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        ListNode listNode3 = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(4))))));
        assertThat(Challenges.mergeTwoLists(listNode1, listNode2), is(listNode3));
    }

    @Test
    public void kaprekarTest() {
        assertThat(Challenges.kaprekar(3524), is(3));
        assertEquals(5, Challenges.kaprekar(1112));
        assertEquals(5, Challenges.kaprekar(100));
        assertEquals(4, Challenges.kaprekar(101));
    }
}
