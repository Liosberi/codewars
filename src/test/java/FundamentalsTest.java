import com.omar.challenge.Fundamentals;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FundamentalsTest {

    @Test
    public void testArrayDiff() {
        assertArrayEquals(new int[]{2}, Fundamentals.arrayDiff(new int[]{1, 2}, new int[]{1}));
        assertArrayEquals(new int[]{2, 2}, Fundamentals.arrayDiff(new int[]{1, 2, 2}, new int[]{1}));
        assertArrayEquals(new int[]{1}, Fundamentals.arrayDiff(new int[]{1, 2, 2}, new int[]{2}));
        assertArrayEquals(new int[]{1, 2, 2}, Fundamentals.arrayDiff(new int[]{1, 2, 2}, new int[]{}));
        assertArrayEquals(new int[]{}, Fundamentals.arrayDiff(new int[]{}, new int[]{1, 2}));
        assertArrayEquals(new int[]{26, 3, 3}, Fundamentals.arrayDiff(new int[]{13, 2, 26, 13, 2, 3, 3}, new int[]{2, 13}));
    }

    @Test
    public void testBoolToWord() {
        assertEquals("Yes", Fundamentals.boolToWord(true));
        assertEquals("No", Fundamentals.boolToWord(false));
    }

    @Test
    public void FixedTests() {
        assertEquals(true, Fundamentals.isIsogram("Dermatoglyphics"));
        assertEquals(true, Fundamentals.isIsogram("isogram"));
        assertEquals(false, Fundamentals.isIsogram("moose"));
        assertEquals(false, Fundamentals.isIsogram("isIsogram"));
        assertEquals(false, Fundamentals.isIsogram("aba"));
        assertEquals(false, Fundamentals.isIsogram("moOse"));
        assertEquals(true, Fundamentals.isIsogram("thumbscrewjapingly"));
        assertEquals(true, Fundamentals.isIsogram(""));
    }


    @Test
    public void testRemoval() {
        assertThat(Fundamentals.remove("Omar"), is("ma"));
        assertEquals("loquen", Fundamentals.remove("eloquent"));
        assertEquals("ountr", Fundamentals.remove("country"));
        assertEquals("erso", Fundamentals.remove("person"));
        assertEquals("lac", Fundamentals.remove("place"));
    }

    @Test
    public void testReverseInteger() {
        assertEquals(0, Fundamentals.reverseInteger(0));
        assertEquals(51, Fundamentals.reverseInteger(15));
        assertEquals(987654321, Fundamentals.reverseInteger(123456789));
        //assertEquals(42145, Fundamentals.sortDesc(54421));
        //assertEquals(145263, Fundamentals.sortDesc(654321));
    }

    @Test
    public void testEndsWith(){
        assertTrue(Fundamentals.endsWhit("abc","bc"));
        assertFalse(Fundamentals.endsWhit("abc","d"));
    }

    @Test
    public void testOdd() {
        assertEquals("odd", Fundamentals.oddOrEven(new int[] {2, 5, 34, 6}));
    }

    @Test
    public void testReverseWords() {
        assertEquals("ehT kciuq nworb xof spmuj revo eht yzal .god", Fundamentals.reverseWords("The quick brown fox jumps over the lazy dog."));
        assertEquals("elppa", Fundamentals.reverseWords("apple"));
        assertEquals("a b c d", Fundamentals.reverseWords("a b c d"));
        assertEquals("elbuod  decaps  sdrow", Fundamentals.reverseWords("double  spaced  words"));
        assertEquals("  ", Fundamentals.reverseWords("  "));
    }

    @Test
    public void TestGetSum()
    {
        assertEquals(-1, Fundamentals.getSum(0, -1));
        assertEquals(1, Fundamentals.getSum(0, 1));
        assertEquals(3, Fundamentals.getSum(1, 2));
        assertEquals(1, Fundamentals.getSum(1, 1));
        assertEquals(2, Fundamentals.getSum(-1, 2));
    }

    @Test
    public void testKeepHydrated() {
        assertEquals(1, Fundamentals.liters(2));
        assertEquals(0, Fundamentals.liters(0.97));
        assertEquals(7, Fundamentals.liters(14.64));
        assertEquals(800, Fundamentals.liters(1600.20));
        assertEquals(40, Fundamentals.liters(80));
    }

    @Test
    public void testCountSmiles() {
        List<String> a = Arrays.asList(":)",":D",":-}",":-()",":xD");
        assertEquals(2, Fundamentals.countSmileys(a));

        List<String> b = new ArrayList<>();
        b.add(":)"); b.add("XD"); b.add(":0}"); b.add("x:-"); b.add("):-"); b.add("D:");
        assertEquals(1, Fundamentals.countSmileys(b));

        List<String> c =  new ArrayList<>();
        c.add(":)"); c.add(":D"); c.add("X-}"); c.add("xo)"); c.add(":X"); c.add(":-3"); c.add(":3");
        assertEquals(2, Fundamentals.countSmileys(a));
 
        List<String> d =  new ArrayList<>();
        d.add(":)"); d.add(":)"); d.add("x-]"); d.add(":ox"); d.add(";-("); d.add(";-)"); d.add(";~("); d.add(":~D");
        assertEquals(4, Fundamentals.countSmileys(d));
    }

    @Test
    public void testFilterWords(){
        assertArrayEquals(new String[]{"amab","ab"},Fundamentals.filterWords(new String[]{"amab","omar","ab"}));
    }

    @Test
    public void testRemoveDuplicates(){
        assertEquals(5,Fundamentals.removeDuplicates(new int[]{0,0,1,1,1,2,2,3,3,4}));
        assertEquals(2,Fundamentals.removeDuplicates(new int[]{1,1,2}));
    }
}